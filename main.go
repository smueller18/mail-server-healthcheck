package main

import "gitlab.com/smueller18/mail-server-healthcheck/cmd"

func main() {
	cmd.Execute()
}
