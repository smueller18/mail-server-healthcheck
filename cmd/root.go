package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/smueller18/mail-server-healthcheck/healthcheck"
	"log"
	"time"
)

var rootCmd = &cobra.Command{
	Use:   "mail-server-healthcheck",
	Short: "Runs mail server healthchecks periodically.",
	Long:  `Runs mail server healthchecks periodically.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		mailConfig, metricsConfig, err := getConfig(cmd)
		if err != nil {
			return err
		}
		healthcheck.Serve(mailConfig, metricsConfig)
		return nil
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

func init() {
	rootCmd.Flags().String("metrics-address", ":8080", "The address of the metrics server.")
	rootCmd.Flags().String("metrics-pattern", "/metrics", "The pattern of the metrics server URL.")
	rootCmd.Flags().Duration("error-retry-interval", time.Minute * 5, "Retry interval when error occurred in function execution.")
	rootCmd.Flags().Duration("count-mail-interval", time.Minute * 10, "Interval to count mails.")
	rootCmd.Flags().Duration("send-mail-interval", time.Minute * 30, "Interval to send mails.")
	rootCmd.Flags().String("host", "", "SMTP host.")
	rootCmd.Flags().Int("smtp-port", 587, "SMTP port.")
	rootCmd.Flags().Int("imap-port", 993, "IMAP port.")
	rootCmd.Flags().String("username", "", "SMTP username.")
	rootCmd.Flags().String("password", "", "SMTP password.")
	rootCmd.Flags().String("sender", "", "E-Mail address of sender.")
	rootCmd.Flags().String("recipient", "", "E-Mail address of recipient.")
	rootCmd.Flags().String("subject", "Healthcheck", "E-Mail subject.")
}

func getConfig(cmd *cobra.Command) (*healthcheck.MailConfig, *healthcheck.MetricsConfig, error) {

	errorRetryInterval, err := cmd.Flags().GetDuration("error-retry-interval")
	if err != nil {
		return nil, nil, err
	}
	countMailInterval, err := cmd.Flags().GetDuration("count-mail-interval")
	if err != nil {
		return nil, nil, err
	}
	sendMailInterval, err := cmd.Flags().GetDuration("send-mail-interval")
	if err != nil {
		return nil, nil, err
	}

	smtpPort, err := cmd.Flags().GetInt("smtp-port")
	if err != nil {
		return nil, nil, err
	}
	imapPort, err := cmd.Flags().GetInt("imap-port")
	if err != nil {
		return nil, nil, err
	}

	return &healthcheck.MailConfig{
			Host:      cmd.Flag("host").Value.String(),
			SmtpPort:  smtpPort,
			ImapPort:  imapPort,
			Username:  cmd.Flag("username").Value.String(),
			Password:  cmd.Flag("password").Value.String(),
			Recipient: cmd.Flag("recipient").Value.String(),
			Sender:    cmd.Flag("sender").Value.String(),
			Subject:   cmd.Flag("subject").Value.String(),
		},
		&healthcheck.MetricsConfig{
			Address:            cmd.Flag("metrics-address").Value.String(),
			Pattern:            cmd.Flag("metrics-pattern").Value.String(),
			ErrorRetryInterval: errorRetryInterval,
			CountMailInterval:  countMailInterval,
			SendMailInterval:   sendMailInterval,
		},
		nil
}
