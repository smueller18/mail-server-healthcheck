package healthcheck

import (
	"crypto/tls"
	"fmt"
	"github.com/emersion/go-imap/client"
	"gopkg.in/gomail.v2"
	"gopkg.in/loremipsum.v1"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

type MailConfig struct {
	Host      string
	SmtpPort  int
	ImapPort  int
	Username  string
	Password  string
	Recipient string
	Sender    string
	Subject   string
}

func SendMail(config *MailConfig) error {

	m := gomail.NewMessage()
	m.SetHeader("From", config.Sender)
	m.SetHeader("To", config.Recipient)
	m.SetHeader("Subject", config.Subject)
	m.SetHeader("Message-Id", fmt.Sprintf("<%s.%d@%s>", time.Now().Format("20060102150405"), rand.Int(), config.Host))

	content := "Hello my friend,\n"
	content += loremipsum.New().Sentences(1)
	content += "\n"
	content += loremipsum.New().Sentences(2)
	content += "\n"
	content += loremipsum.New().Sentences(1)
	content += "\n"
	content += "Have a nice weekend"

	m.SetBody("text/plain", strings.ReplaceAll(content, "\n", "\n\r"))
	m.AddAlternative("text/html", strings.ReplaceAll(content, "\n", "<br /><br />"))

	d := gomail.NewDialer(config.Host, config.SmtpPort, config.Username, config.Password)

	if err := d.DialAndSend(m); err != nil {
		return err
	}
	return nil

}

func CountMails(config *MailConfig) (uint32, error) {
	tlsconfig := &tls.Config{
		InsecureSkipVerify: false,
		ServerName:         config.Host,
	}
	c, err := client.DialTLS(config.Host+":"+strconv.Itoa(config.ImapPort), tlsconfig)
	if err != nil {
		return 0, err
	}

	defer c.Logout()

	if err := c.Login(config.Username, config.Password); err != nil {
		return 0, err
	}

	mbox, err := c.Select("INBOX", false)
	if err != nil {
		return 0, err
	}

	return mbox.Messages, nil

}
