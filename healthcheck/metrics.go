package healthcheck

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"log"
	"net/http"
	"time"
)

type MetricsConfig struct {
	Address            string
	Pattern            string
	ErrorRetryInterval time.Duration
	CountMailInterval  time.Duration
	SendMailInterval   time.Duration
}

var inboxTotal = prometheus.NewGaugeVec(
	prometheus.GaugeOpts{
		Name: "mail_inbox_total",
		Help: "Count of mails in INBOX",
	},
	[]string{"mail_host", "mail_user"},
)

var errors = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "mail_healthcheck_errors_total",
		Help: "Total number of errors when executing healthchecks",
	},
	[]string{"mail_host", "mail_sender", "mail_recipient"},
)

func init() {
	prometheus.MustRegister(inboxTotal)
	prometheus.MustRegister(errors)
}

func Serve(mailConfig *MailConfig, metricsConfig *MetricsConfig) {
	errors.WithLabelValues(mailConfig.Host, mailConfig.Sender, mailConfig.Recipient)

	go func() {
		for {
			if err := SendMail(mailConfig); err != nil {
				errors.WithLabelValues(mailConfig.Host, mailConfig.Sender, mailConfig.Recipient).Inc()
				log.Printf("SendMail: %s", err)
				time.Sleep(metricsConfig.ErrorRetryInterval)
			} else {
				log.Printf("SendMail: Successfully send mail to %s\n", mailConfig.Recipient)
				time.Sleep(metricsConfig.SendMailInterval)
			}
		}
	}()

	go func() {
		for {
			count, err := CountMails(mailConfig)
			if err != nil {
				errors.WithLabelValues(mailConfig.Host, mailConfig.Sender, mailConfig.Recipient).Inc()
				log.Printf("CountMails: %s", err)
				time.Sleep(metricsConfig.ErrorRetryInterval)
			} else {
				inboxTotal.WithLabelValues(mailConfig.Host, mailConfig.Username).Set(float64(count))
				log.Printf("CountMails: Current mail count is %d\n", count)
				time.Sleep(metricsConfig.CountMailInterval)
			}
		}
	}()

	http.Handle(metricsConfig.Pattern, promhttp.Handler())
	log.Fatal(http.ListenAndServe(metricsConfig.Address, nil))

}
