module gitlab.com/smueller18/mail-server-healthcheck

go 1.13

require (
	github.com/emersion/go-imap v1.0.1
	github.com/prometheus/client_golang v1.2.1
	github.com/spf13/cobra v0.0.5
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	gopkg.in/loremipsum.v1 v1.0.0
)
