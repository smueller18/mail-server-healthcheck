FROM golang:1-alpine as builder

WORKDIR /go/src/gitlab.com/smueller18/mail-server-healthcheck

COPY go.mod go.sum ./
RUN go mod download

COPY . .
ENV CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64
RUN go build -v -ldflags "-s -w" -o mail-server-healthcheck

FROM alpine:3.10
RUN apk --update upgrade \
    && apk --no-cache --no-progress add ca-certificates

COPY --from=builder /go/src/gitlab.com/smueller18/mail-server-healthcheck/mail-server-healthcheck /usr/bin/mail-server-healthcheck

ENTRYPOINT ["/usr/bin/mail-server-healthcheck"]